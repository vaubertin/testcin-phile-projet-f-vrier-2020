"""
Ce programme demande le nombre de participants au test et recommence le test pour chaque participants. Le test consiste à répondre à un certain nombre de questions sur le cinéma(en l'occurence 5 mais on peut en 
rajouter en complétant correctement les variables 'questions', 'réponses_possibles' et 'bonnes_réponses, le programme s'adapte au nombre de questions).
Après le test, la moyenne sur 20 points des questions est communiqué au participant du test.
Après que tous les participants aient fait leur test, tous les scores des participants sont renvoyés dans le terminal et un podium est dressé.

Mode d'emploi : il suffit de lancer le programme en cliquant sur la flèche verte en haut à droite ou en écrivant 'python TestCinéphile.py' dans le terminal (si vous vous trouvez dans le document où le fichier 
'TestCinéphile.py' se situe). Il faut installer easygui grâce à son terminal en entrant : $ pip install easygui .

auteur: V.AUBERTIN
"""
import easygui as eg 
from typing import List, Dict, Tuple, Set
from math import inf
titre:str="Test cinéphile"          #Variable contenant le titre qui va apparaître dans les interfaces easygui.
questions:List[str]=["Première question, qui est l'acteur du personnage éponyme du film Forest Gump ?",         #Liste des questions qui vont être posées aux participants à travers une interface easygui.
"Seconde question, qui est le réalisateur du film Alien ?",
"Plus difficile, combien y a-t-il de films, sortis au cinéma sur l'univers de Star Wars ?",
"Dans lequel de ces films, Johnny Depp n'a-t-il jamais joué ?",
"Laquelle de ces célébrités n'a jamais joué dans un film ?"]
réponses_possibles:List[Tuple[str]]=[("Tom Hanks","Leonardo Di Caprio","Brad Pitt","Georges Clooney"),          #Liste contenant les réponses possibles, réunis par un tuple, des questions.
("Oliver Stone","Tim Burton","Xavien Dolan","Ridley Scott"),            #Le participant devra appuyer sur un des quatres boutons de l'interface easygui ayant chacun pour étiquette ces réponses possibles.
("9","10","11","12"),
("Edward aux mains d'argent","Les animaux fantastiques","Maléfique","Sleepy Hollow"),
("Orelsan","Gaël Monfils","Frank Leboeuf","Julien Doré")]
bonnes_réponses:Set[str]={"Tom Hanks","Ridley Scott","12","Maléfique","Gaël Monfils"}           #Ensemble contenant les bonnes réponses aux questions.
phrases_fin_de_test:Tuple[str]=("\nL'important, c'est de participer ...",           #Tuple contenant les phrases de fin de test qui vont être affichées à la fin de chaque test avec la note du participant.
"\nC'est toujours mieux que d'avoir 0 ...",         #Les phrases sont affichés selon la note du participants.
"\nFélicitations, la moyenne est 'à l'abri !",
"\nBravo, vous êtes un vrai cinéphile !")
classement_non_trié:Dict[str,int]={}            #Dictionnaire provisoirement vide qui va contenir les résultats des participants. Chaque clé correspondra au pseudo du joueur et aura comme valeur son score.
classement_trié:List[Tuple[str,int]]=[]         #Liste de couples provisoirement vide qui contiendra les résultats des participants triés du score le plus haut au score le plus bas.

def comptage(structure)->int:
    """
    Correspond à la fonction len.
    """
    compteur:int=0          
    for truc in structure:
        compteur+=1
    return(compteur)

def maxi(dico:Dict[str,int])->Tuple[str,int]:
    """
    Correspond à la fonction max.
    """
    maximum:int=-inf
    pseudo_max:str=""
    for pseudo in dico.keys():
        if dico[pseudo]>maximum:
            maximum=dico[pseudo]
            pseudo_max=pseudo
    return(pseudo_max,maximum)

def msg(message:str)->None:         #En entrée, le message que l'on veut afficher.
    """
    Cette fonction est une fonction raccourcie car, dans ce programme, la fonction eg.msgbox (easygui.msgbox) est beaucoup utilisé.
    Cette fonction nous permet de gagner du temps avec un nom d'appel court (msg) et nous ne sommes pas obligés de mettre la variable titre en paramètre car il sera automatiquement mis.
    """
    eg.msgbox(message,titre)            #Au lieu d'écrire eg.msgbox(message,titre), on écrira msg(message).

def test()->int:
    """
    Cette fonction pose toutes les questions présentes dans la liste 'questions' à l'utilisateur qui doit répondre en appuyant sur un bouton. La fonction vérifie si les réponses sont correctes et calcule la moyenne 
    sur 20. La fonction communique, au participant, sa moyenne avec une phrase plus ou moins arrogante selon la moyenne qu'il a eu.
    """
    msg("Bienvenue dans le test cinéphile, vous aurez 4 propositions par question et une seule est correcte.")          #Introduction au test au participant.
    moyenne:int=0           #La moyenne est définit nulle 
    for x in range(comptage(questions)):         #Pour chaque questions:
        réponse_donnée:str=eg.buttonbox(questions[x], titre, choices=réponses_possibles[x])         #Propose les réponses possibles correspondant à la question et récolte la réponse donnée par le participant.
        if réponse_donnée in bonnes_réponses:           #Si la réponse est dans la variable des bonnes réponses :
            moyenne+=1          #La moyenne gagne 1 point .
    moyenne*=20/(comptage(questions))            #Met la moyenne sur 20 points.
    msg_fin:str="Ton score est de "+str(moyenne)+"/20.0  ."         #Définit le message de fin de test affichant la moyenne du participant.
    if moyenne<5:                              
        msg(msg_fin+phrases_fin_de_test[0])
    if moyenne>=5 and moyenne<10:
        msg(msg_fin+phrases_fin_de_test[1])          #Selon la moyenne obtenue par le participant, une phrase de fin, stockée dans la variable phrases_fin_de_test, sera ajoutée au message de fin.
    if moyenne>=10 and moyenne<15:
        msg(msg_fin+phrases_fin_de_test[2])
    if moyenne>=15:
        msg(msg_fin+phrases_fin_de_test[3])
    return(moyenne)             

nombres_participants_possibles:List[int]=[]         #Liste provisoirement vide des nombres possibles de participants.
for x in range(1,101):                               #Ajoute dans la liste les nombres de 1 à 100 sous forme de str. Les nombres possibles de participants sont donc dans la liste 'nombres_participants_possibles et vont de 1 à 100. 
    nombres_participants_possibles.append(str(x))          #Grâce à cette liste, si au moment où le nombre de participants sera rentré, toutes les valeurs qui ne sont pas des chiffres hormis 0 seront écartés.         
nombre_participants:str=eg.enterbox("Combien êtes-vous de participants (maximum 100 participants) ?", titre)          #Demande le nombre de participants.
while nombre_participants not in nombres_participants_possibles:            #Tant que le nombre de participants n'est pas dans la liste des nombres de participants possibles :
    msg("Ce nombre de participants est incorrect.")         #Affiche un message d'erreur  
    nombre_participants=eg.enterbox("Combien êtes-vous de participants (maximum 100 participants) ?", titre)          #Et redemande le nombre de participants.

for x in range(int(nombre_participants)):           #Pour chaque participants:
    pseudo:str=eg.enterbox("Entrez un pseudonyme.", titre)          #Demande un pseudonyme
    while pseudo in classement_non_trié.keys():         #Tant que ce pseudo est déjà pris          
        msg("Ce pseudo est déjà pris ...")          #Affiche un message d'erreur.
        pseudo=eg.enterbox("Entrez un pseudonyme.", titre)          #Et redemande un pseudo. 
    resultat:int=test()        #La variable résultat devient la moyenne du participant.
    classement_non_trié[pseudo]=resultat         #On attribue le dictionnaire.

while comptage(classement_non_trié)>0:          #Tant que la variable ayant reçu les résultats des participants n'est pas vide.
    meilleur_joueur:Tuple[str,int]=maxi(classement_non_trié)           #Trouve le meilleur joueur c'est à dire le participant avec le score le plus haut du classement grâce à la fonction maxi.
    classement_trié.append(meilleur_joueur)         #Ajoute en fin de la liste classement_trié le meilleur joueur.
    del classement_non_trié[meilleur_joueur[0]]         #Supprime du dictionnaire classement_non_trié les données du meilleur joueur.

for x in range(int(nombre_participants), 0, -1):            #Pour x allant du nombre de participants à 1 avec un pas de -1:
    if x==1:            #Si x vaut 1:
        msg("À la 1ère place, il y a "+classement_trié[0][0]+" avec une note de "+str(classement_trié[0][1])+"/20.")            #Affiche le premier élément du premier tuple de la liste c'est-à-dire le pseudo du participant ayant le score le plus haut. Affiche aussi son score et sa place (en l'occurrence 1er).
    else:           #Sinon c'est à dire si x vaut tout sauf 1:
        msg("À la "+str(x)+"ème place il y a "+classement_trié[x-1][0]+" avec une note de "+str(classement_trié[x-1][1])+"/20.")            #Si x vaut 2 : affiche le pseudo du participant ayant le deuxième score le plus haut ainsi que son score et sa place (2ème). Si x vaut 3 : Affiche le pseudo du participant ayant le troisième score le plus haut ainsi que son score et sa place (3ème). etc...

for couple in classement_trié:          #Pour chaque couple (pseudo,score) dans la liste classement_trié:
    print(couple)           #Affiche dans le terminal le couple.