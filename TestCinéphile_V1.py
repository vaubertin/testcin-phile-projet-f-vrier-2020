"""
Ce programme demande le nombre de participants au test et recommence le test pour chaque participants. Le test consiste à répondre à un certain nombre de questions sur le cinéma(en l'occurence 5 mais on peut en 
rajouter en complétant correctement les variables 'questions', 'réponses_possibles' et 'bonnes_réponses. Le programme s'adapte au nombre de questions).
Après le test, la moyenne sur 20 points des questions est communiqué au participant du test.
Après que tous les participants aient fait leur test, tous les scores des participants sont renvoyés dans le terminal.

Mode d'emploi : il suffit de lancer le programme en cliquant sur la flèche verte en haut à droite ou en écrivant 'python TestCinéphile.py' dans le terminal (si vous vous trouvez dans le document où le fichier 
'TestCinéphile.py' se situe).

auteur: V.AUBERTIN
"""
import easygui as eg 
from typing import List, Dict, Tuple, Set
titre:str="Test cinéphile"
questions:List[str]=["Première question, qui est l'acteur du personnage éponyme du film Forest Gump ?",
"Seconde question, qui est le réalisateur du film Alien ?",
"Plus difficile, combien y a-t-il de films, sortis au cinéma sur l'univers de Star Wars ?",
"Dans lequel de ces films, Johnny Depp n'a-t-il jamais joué ?",
"Laquelle de ces célébrités n'a jamais joué dans un film ?"]
réponses_possibles:List[Tuple[str]]=[("Tom Hanks","Leonardo Di Caprio","Brad Pitt","Georges Clooney"),
("Oliver Stone","Tim Burton","Xavien Dolan","Ridley Scott"),
("9","10","11","12"),
("Edward aux mains d'argent","Les animaux fantastiques","Maléfique","Sleepy Hollow"),
("Orelsan","Gaël Monfils","Frank Leboeuf","Julien Doré")]
bonnes_réponses:Set[str]={"Tom Hanks","Ridley Scott","12","Maléfique","Gaël Monfils"}
phrases_fin_de_test:Tuple[str]=("\nL'important, c'est de participer ...",
"\nC'est toujours mieux que d'avoir 0 ...",
"\nFélicitations, la moyenne est 'à l'abri !",
"\nBravo, vous êtes un vrai cinéphile !")

def msg(message:str)->None:         #En entrée, le message que l'on veut afficher. (Rien en sortie)
    """
    Cette fonction est une fonction raccourcie car, dans ce programme, la fonction eg.msgbox (easygui.msgbox) est beaucoup utilisé.
    Cette fonction nous permet de gagner du temps avec un nom d'appel court (msg) et nous ne sommes pas obligés de mettre la variable titre en paramètre car il sera automatiquement mis.
    """
    eg.msgbox(message,titre)            #Au lieu d'écrire eg.msgbox(message,titre), on écrira msg(message).

def test()->int:
    """
    Cette fonction pose toutes les questions présentes dans la liste 'questions' à l'utilisateur qui doit répondre en appuyant sur un bouton. La fonction vérifie si les réponses sont correctes et calcule la moyenne 
    sur 20. La fonction communique, au participant, sa moyenne avec une phrase plus ou moins arrogante selon la moyenne qu'il a eu.
    """
    msg("Bienvenue dans le test cinéphile, vous aurez 4 propositions par question et une seule est correcte.")          #Introduction au test au participant.
    moyenne:int=0           #La moyenne est définit nulle 
    for x in range(len(questions)):         #Pour chaque questions:
        réponse_donnée:str=eg.buttonbox(questions[x], titre, choices=réponses_possibles[x])         #Propose les réponses possibles correspondant à la question et récolte la réponse donnée par le participant.
        if réponse_donnée in bonnes_réponses:           #Si la réponse est dans la variable des bonnes réponses :
            moyenne+=1          #La moyenne gagne 1 point .
    moyenne*=20/(len(questions))            #Met la moyenne sur 20 points.
    msg_fin:str="Ton score est de "+str(moyenne)+"/20.0  ."         #Définit le message de fin de test affichant la moyenne du participant.
    if moyenne<5:                              
        msg(msg_fin+phrases_fin_de_test[0])
    if moyenne>=5 and moyenne<10:
        msg(msg_fin+phrases_fin_de_test[1])          #Selon la moyenne obtenue par le participant, une phrase de fin, stockée dans la variable phrases_fin_de_test, sera ajoutée au message de fin.
    if moyenne>=10 and moyenne<15:
        msg(msg_fin+phrases_fin_de_test[2])
    if moyenne>=15:
        msg(msg_fin+phrases_fin_de_test[3])
    return(moyenne)             

nombres_participants_possibles:List[int]=[]         #Liste provisoirement vide des nombres possibles de participants.
for x in range(1,10):                               #Ajoute dans la liste les nombres de 1 à 9 sous forme de str. Les nombres possibles de participants sont donc dans la liste 'nombres_participants_possibles et  
    nombres_participants_possibles+=str(x)          #vont de 1 à 9. Grâce à cette liste, si au moment où le nombre de participants sera rentré, toutes les valeurs qui ne sont pas des chiffres hormis 0 seront écartés.         
nombre_participants:str=eg.enterbox("Combien êtes-vous de participants (maximum 9 participants) ?", titre)          #Demande le nombre de participants.
while nombre_participants not in nombres_participants_possibles:            #Tant que le nombre de participants n'est pas dans la liste des nombres de participants possibles :
    msg("Ce nombre de participants est incorrect.")         #Affiche un message d'erreur  
    nombre_participants=eg.enterbox("Combien êtes-vous de participants (maximum 9 participants) ?", titre)          #Et redemande le nombre de participants.

for x in range(int(nombre_participants)):           #Pour chaque participants:
    pseudo=eg.enterbox("Entrez un pseudonyme.", titre)          #Demande un pseudonyme
    resultat:int=test()        #La variable résultat devient la moyenne du participant.
    résultat_final:Dict[str,int]={}     #On créé un dictionnaire qui va devenir le réceptacle de la moyenne(valeur) du participant(clé).
    résultat_final[pseudo]=resultat         #On attribue le dictionnaire
    print(résultat_final)           #On renvoie le dictionnaire. Au final, tous les scores des participants seront renvoyés avec leur nom.